<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta name="author" content="Tobias Schneider" />
  <meta
    content="autonomous driving, user experience, tobias schneider, autonomous driving experience cards, cards, card based, experience, ADX.cards"
    name="keywords" />
  <title>Autonomous Driving Experience Cards</title>

  <link rel="stylesheet" href="css/milligram.min.css" />
  <link rel="stylesheet" href="css/main.css" />
  <link rel="stylesheet" href="css/styles.css" />
  <link rel="icon" type="image/x-icon" href="img/favicon.ico">
</head>


<body>
  <main class="wrapper">
    <nav class="navbar">
      <label class="navbar-toggle" id="js-navbar-toggle" for="chkToggle">
        <i class="">☰</i>
      </label>
      <a href="index.html" class="logo">ADX.cards</a>
      <input type="checkbox" id="chkToggle"></input>
      <ul class="main-nav" id="js-menu">
        <li><a class="navigation-link" href="game-plan.html">Game Plan</a></li>
        <li><a class="navigation-link" href="knowledge-cards.html">Knowledge Cards</a></li>
        <li><a class="navigation-link" href="feedback-modality-cards.html">Feedback Modality Cards</a></li>
        <li><a class="navigation-link" href="evaluation-strategy-cards.html">Evaluation Strategy Cards</a></li>
      </ul>
    </nav>

    <div id="header-sub-img"><img src="img/statistics.png" /></div>

    <section class="container">


      <h1 class="title center">Statistical Comparison and Significance</h1>

      <p class="spacer"></p>
      <p class="spacer"></p>

      <p class="quote">
        “Quantitative research is great for determining the scale or priority of design problems, benchmarking the
        experience, or comparing different design alternatives in an experimental way.” [1]</p>

      <p class="spacer"></p>

      <ul>
        <li>We recommend performing A/B testing, for example using groups with and without transparency-communication,
          to better understand the results, see how much they differ and if their difference is significant. </li>
        <li>For statistical comparison of differential hypotheses – comparing the results of two or more groups – we
          recommend the following steps to find the correct statistical method:</li>
      </ul>

      <p>💡 To calculate the statistical significance with the online calculators, paste <span class="highlight">all
          raw-data of every tester</span> into the calculators. E.g. all UEQ-S likert scale answers from testers of
        group 1 into the first column and all data from group 2 into the second column.</p>

      <p class="spacer"></p>

      <p class="callout center">To know which statistical methods to use you have to <a
          href="https://www.socscistatistics.com/tests/kolmogorov/default.aspx" target="_blank">check your data for
          normal distribution first</a> and see if they are <a
          href="https://www.formpl.us/blog/interval-data">interval-scaled</a>.</p>

      <p class="center"><i class="arrow down"></i></p>

      <p class="callout center">If your data is normally distributed and interval-scaled you should use parametric
        tests. We recommend:</p>

      <div class="row">
        <div class="column">
          <p class="callout gray center">
            <a href="https://www.socscistatistics.com/tests/studentttest/default.aspx" target="_blank">t-test</a> for
            two dependent or independent samples
          </p>
        </div>
        <div class="column">
          <p class="callout gray center">ANOVA for three or more
            samples (<a href="https://www.socscistatistics.com/tests/anovarepeated/default.aspx"
              target="_blank>">dependent</a> or <a href="https://www.socscistatistics.com/tests/anova/default2.aspx"
              target="_blank>">independent</a>)</p>
        </div>
      </div>

      <p class="highlight center">💡 Dependent samples are from the same set of individuals. Independent samples are
        from different sets of individuals.</p>

      <p class="center"><i class="arrow down"></i></p>

      <p class="callout center">If your data is not normally distributed or not metric you should use non-parametric
        tests. We recommend:</p>

      <div class="row">
        <div class="column">
          <p class="callout gray center">
            <a href="https://www.socscistatistics.com/tests/signedranks/default.aspx" target="_blank">Wilcoxon Signed
              Rank test</a> for dependent samples
          </p>
        </div>
        <div class="column">
          <p class="callout gray center"><a href="https://www.socscistatistics.com/tests/mannwhitney/default2.aspx"
              target="_blank>">Mann-Whitney test</a> for independent <br>samples</p>
        </div>
      </div>

      <p class="spacer"></p>

      <p class="center">💡 It depends on your hypothesis if you have to use one-tailed or two-tailed testing [3]:</p>
      <ul>
        <li><span class="highlight">One-tailed</span> testing is used for a <span class="highlight">directional
            hypothesis</span>. For example: “Using light as feedback modality will increase the feeling of control
          compared to no feedback.”</li>
        <li><span class="highlight">Two-tailed</span> testing is used for a <span class="highlight">non-directional
            hypothesis</span>. For example: “There is a difference in the feeling of control regarding light as a
          feedback modality and no feedback.”</li>
      </ul>


      <div class="divider"></div>


      <h2 class="center">Statistical Significance </h2>


      <p class="highlight"><b>When using statistical methods for your hypotheses you want their results to be
          significant at p<.05 or even p<.01.</b>
      </p>

      <p>"Statistical significance" refers to the probability that the observed result could have occurred randomly if
        it has no true underlying effect. This probability is usually referred to as "p" and by convention, <span
          class="highlight">p should be smaller than 5% to consider a finding significant. </p>

      <p>Sometimes researchers insist on stronger significance and want p to be smaller than 1%, or even 0.1%, before
        they'll accept a finding with wide-reaching consequences, say, for a new blood-pressure medication to be taken
        by millions of patients.” [2]</p>

      <p class="spacer"></p>
      <p class="spacer"></p>

      <div class="row">
        <div class="column">
          <p class="center"><a class="switch-card" href="knowledge-qual-quant.html">Previous Card</a></p>
        </div>
        <div class="column">
          <p class="center"><a class="switch-card" href="knowledge-prototypes.html">Next Card</a></p>
        </div>
      </div>


      <div class="divider"></div>


      <h2 class="center">Sources</h2>

      <p class="source">[1] - Moran, Kate. "Quantitative Research: Study Guide" URL:
        https://www.nngroup.com/articles/quantitative-research-study-guide/[Accessed September 2022] 8 (2021).</p>
      <p class="source">[2] - Nielsen, Jakob. "Understanding Statistical Significance" URL:
        http://www.nngroup.com/articles/usability-101-introduction-to-usability/[Accessed September 2022] 3 (2014).</p>
      <p class="source">[3] - Surbhi, S. "Difference Between One-tailed and Two-tailed Test" URL:
        https://keydifferences.com/difference-between-one-tailed-and-two-tailed-test.html[Accessed September 2022] 2
        (2018).</p>


    </section>


    <footer class="footer">
      <section class="container">
        <p class="center">
          Framework by <a href="https://tobiasschneider.net" target="_blank">Tobias Schneider</a> ©2022 - present | <a
            href="image-credits.html">Image Credits</a> | <a href="changelog.html">Changelog</a>
        </p>
      </section>
    </footer>
  </main>

</body>

</html>