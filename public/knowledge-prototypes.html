<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta name="author" content="Tobias Schneider" />
  <meta
    content="autonomous driving, user experience, tobias schneider, autonomous driving experience cards, cards, card based, experience, ADX.cards"
    name="keywords" />
  <title>Autonomous Driving Experience Cards</title>

  <link rel="stylesheet" href="css/milligram.min.css" />
  <link rel="stylesheet" href="css/main.css" />
  <link rel="stylesheet" href="css/styles.css" />
  <link rel="icon" type="image/x-icon" href="img/favicon.ico">
</head>


<body>
  <main class="wrapper">
    <nav class="navbar">
      <label class="navbar-toggle" id="js-navbar-toggle" for="chkToggle">
        <i class="">☰</i>
      </label>
      <a href="index.html" class="logo">ADX.cards</a>
      <input type="checkbox" id="chkToggle"></input>
      <ul class="main-nav" id="js-menu">
        <li><a class="navigation-link" href="game-plan.html">Game Plan</a></li>
        <li><a class="navigation-link" href="knowledge-cards.html">Knowledge Cards</a></li>
        <li><a class="navigation-link" href="feedback-modality-cards.html">Feedback Modality Cards</a></li>
        <li><a class="navigation-link" href="evaluation-strategy-cards.html">Evaluation Strategy Cards</a></li>
      </ul>
    </nav>

    <div id="header-sub-img"><img src="img/prototypes.png" /></div>

    <section class="container">


      <h1 class="title center">Types of Prototypes</h1>

      <p class="spacer"></p>
      <p class="spacer"></p>

      <h2 class="center">Definition</h2>

      <p>
      <ul>
        <li>There are many reasons why prototyping is a valuable way of evaluating concepts during development.</li>
        <li>Different prototyping techniques enable gathering user feedback and evaluating the user experience of
          autonomous driving systems.</li>
        <li>Using different prototypes, you can simulate and evaluate scenarios that may be challenging or dangerous in
          real-world testing. This iterative approach allows for identifying and rectifying issues before deploying the
          technology on actual roads.</li>
        <li>Utilising prototypes such as wizard-of-oz simulations, driving simulators, and virtual reality environments
          can significantly reduce costs and development time compared to physical testing with real vehicles.
          Prototyping allows for rapid iterations, quick modifications, and evaluation of different design choices
          without the need for expensive hardware or the risks associated with real-world testing.</li>
        <li>The use of different prototypes allows for an iterative development and validation process. Researchers and
          developers can start with simpler prototypes like wizard-of-oz, gradually progress to driving simulators, and
          ultimately transition to real-world testing. Each iteration helps refine the system, validate its
          capabilities, and uncover potential issues or areas for improvement.</li>
      </ul>
      </p>

      <div class="divider"></div>

      <h2 class="center">Physical Driving Simulator</h2>

      <p>
        Simulators offer a controlled environment to simulate various driving scenarios and conditions. They provide a
        realistic representation of the driving experience, including vehicle dynamics, traffic, and road conditions.
        Using a driving simulator allows researchers and developers to test autonomous driving algorithms, assess their
        performance, and fine-tune the system in a safe and reproducible manner.</p>

      <p class="center"><img src="img/prototypes-simulator.png" style="max-width: 500px; width: 100%"/></p>
      <p class="center">Physical driving simulator used in <a href="https://dl.acm.org/doi/10.1145/3411764.3446647" target="_blank">Schneider, Tobias, et al. "Explain yourself! transparency for positive ux in autonomous driving." Proceedings of the 2021 CHI Conference on Human Factors in Computing Systems. 2021.</a></p>


      <table>
        <thead>
          <tr>
            <th>Advantage</th>
            <th>Disadvantage</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><b>Safe Testing Environment:</b> Driving simulators provide a controlled and safe environment to test autonomous driving algorithms and systems without the risks associated with real-world testing. Developers can simulate various driving scenarios, road conditions, and traffic situations to evaluate the system's performance.</td>
            <td><b>Unrealistic Driving Experience:</b> Despite efforts to create realistic driving scenarios, simulators may not fully replicate the physical sensations and cognitive load experienced during actual driving. This can affect user feedback and the system's performance evaluation, as the human operator's behavior may differ from real-world driving.</td>
          </tr>
          <tr>
            <td><b>Reproducibility and Standardisation:</b> Simulators offer a high level of reproducibility, allowing researchers and developers to conduct tests under consistent conditions. This enables the comparison of different algorithms, configurations, and improvements in a standardised manner.</td>
            <td><b>User Acceptance and Adaptation:</b> The experience of driving in a simulator may differ significantly from driving a real vehicle. Users, particularly those unfamiliar with simulators, may take time to adapt to the interface, controls, and overall experience, potentially affecting their feedback and evaluation of the autonomous driving system.</td>
          </tr>
        </tbody>
      </table>



      <div class="divider"></div>



      <h2 class="center">Virtual Reality Driving Simulator</h2>

      <p>
        VR technologies can create immersive and realistic driving scenarios, allowing users to experience autonomous driving in a very immersive environment. VR provides a cost-effective alternative to physical testing, enabling developers to simulate various road conditions, traffic situations, and potential hazards. It also allows for rapid iterations and testing of user interfaces and human-machine interactions.
</p>

      <p class="center"><img src="img/prototypes-vr.png" style="max-width: 500px; width: 100%;"/></p>
      <p class="center">VR prototype used in <a href="https://dl.acm.org/doi/10.1145/3397481.3450687" target="_blank">Schneider, Tobias, et al. "Increasing the User Experience in Autonomous Driving through different Feedback Modalities." 26th International Conference on Intelligent User Interfaces. 2021.</a></p>


      <table>
        <thead>
          <tr>
            <th>Advantage</th>
            <th>Disadvantage</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><b>Immersive and Realistic Experience:</b> VR driving simulators provide a highly immersive and realistic driving experience by creating a virtual environment. Users can experience the perception of driving in a simulated autonomous vehicle, including visual and audio feedback, which closely resembles real-world driving scenarios.</td>
            <td><b>Cost and Setup Complexity:</b> VR driving simulators can require significant investment in hardware, software, and infrastructure. The setup and calibration of VR systems, including headsets, motion trackers, and computing resources, can be complex and time-consuming.</td>
          </tr>
          <tr>
            <td><b>Safe and Controlled Testing:</b> Similar to traditional driving simulators, VR driving simulators provide a safe and controlled environment for testing autonomous driving systems. Developers can assess the system's performance, evaluate different algorithms, and test various scenarios without the risks associated with real-world testing.</td>
            <td><b>Simulator Sickness:</b> Some users may experience discomfort or simulator sickness, commonly known as motion sickness, when using VR simulators. The sensory disconnect between virtual motion and physical motion can lead to discomfort, which may impact the validity and reliability of user feedback and system evaluation.</td>
          </tr>
          <tr>
            <td><b>Reproducibility and Standardization:</b> VR simulators offer a high level of reproducibility, allowing tests to be conducted under consistent conditions. This enables developers to compare different algorithms, configurations, and improvements in a standardized manner and track the progress of the autonomous driving system.</td>
            <td><b>Learning Curve and Adaptation:</b> Users may require time to adapt to VR environments and become familiar with the controls and interactions within the simulation. The initial learning curve and adjustment period can impact the user's feedback and potentially introduce bias or limitations in the evaluation process.</td>
          </tr>
        </tbody>
      </table>



      <div class="divider"></div>

      <h2 class="center">Wizard-of-Oz with a Real Vehicle</h2>

      <p>
        The Wizard-of-Oz technique allows for the simulation of autonomous driving by having a human "wizard" control the vehicle behind the scenes while giving the illusion of an automated system. This approach is helpful as it provides a low-cost and flexible way to gather user feedback and test the overall user experience. Furthermore, it allows participants to experience a real-world drive which can lead to higher immersion.</p>

      <p class="center"><img src="img/prototypes-wizard.jpeg" style="max-width: 500px; width: 100%;"/></p>
      <p class="center">Passenger shielded from driver, setup used in <a href="https://dl.acm.org/doi/fullHtml/10.1145/3581641.3584085" target="_blank">Schneider, Tobias, et al. "Don’t fail me! The Level 5 Autonomous Driving Information Dilemma regarding Transparency and User Experience." Proceedings of the 28th International Conference on Intelligent User Interfaces. 2023.</a></p>


      <table>
        <thead>
          <tr>
            <th>Advantage</th>
            <th>Disadvantage</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><b>High Immersion:</b> Participants feel the vehicle with all driving forces and do not have to imagine that they are driving around. Driving situations might have a higher impact on participants as they are fully present in the environment and not solely placed in front of a screen.</td>
            <td><b>Scalability Challenges:</b> Wizard-of-Oz prototypes may not scale well when testing with a large number of users. The need for human operators and individualized interactions can make it time-consuming and impractical to gather feedback from a diverse and extensive user base.</td>
          </tr>
          <tr>
            <td><b>Cost-Effective:</b> Wizard-of-Oz prototypes require minimal hardware and development resources compared to building a fully functional autonomous driving system. This makes it a cost-effective option for early-stage testing and gathering user feedback.</td>
            <td><b>Operator Bias and Reproducibility:</b> The behaviour and decisions of the human operator (wizard) may introduce unintentional biases and inconsistencies in the simulation, potentially affecting the validity and reliability of the collected data and user feedback.</td>
          </tr>
        </tbody>
      </table>




      <p class="spacer"></p>
      <p class="spacer"></p>

      <div class="row">
        <div class="column">
          <p class="center"><a class="switch-card" href="knowledge-statistics.html">Previous Card</a></p>
        </div>
      </div>


      <div class="divider"></div>


      <h2 class="center">Sources</h2>

      <p class="source">[1] - Nielsen, Jakob. "Usability 101: Introduction to usability (2012)." URL:
        http://www.nngroup.com/articles/usability-101-introduction-to-usability/[Accessed September 2022] 9 (2012): 35.
      </p>


    </section>


    <footer class="footer">
      <section class="container">
        <p class="center">
          Framework by <a href="https://tobiasschneider.net" target="_blank">Tobias Schneider</a> ©2022 - present | <a
            href="image-credits.html">Image Credits</a> | <a href="changelog.html">Changelog</a>
        </p>
      </section>
    </footer>
  </main>

</body>

</html>